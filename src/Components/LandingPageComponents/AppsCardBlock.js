import React from 'react'
import '../LandingPageComponentCss/AppsCardBlock.css'

const AppsCardBlock = () => {
    return (
        <div className="apps-card-block">
            <h1>Powerful apps for your stories.</h1>
            <p>The world's biggest influencers, creators, publishers, startups and brands use<br />Creatosaurus in their marketing strategy.</p>
            <div className="email-container">
                <input type="text" placeholder="Enter your email" />
                <button>Get Started for Free</button>
            </div>
            <div className="slider">
                <div className="card">
                    <div className="box" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>

                <div className="card">
                    <div className="box box1" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>

                <div className="card">
                    <div className="box box2" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>

                <div className="card">
                    <div className="box box3" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>

                <div className="card">
                    <div className="box box4" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>

                <div className="card">
                    <div className="box box5" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>

                <div className="card">
                    <div className="box box6" />
                    <span>Apollo</span>
                    <span>Online video editor</span>
                </div>
            </div>
        </div>
    )
}

export default AppsCardBlock
