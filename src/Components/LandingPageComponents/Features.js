import React from 'react'
import '../LandingPageComponentCss/Features.css'

const Features = () => {

    const data = [{
        emoji: "✍🏻",
        title: "Marketing Copy Generator",
    }, {
        emoji: "🖼",
        title: "Create Beautiful Graphics",
    }, {
        emoji: "📂",
        title: "Upload & Organise",
    }, {
        emoji: "🎬",
        title: "Easy Video Editor",
    }, {
        emoji: "🎶",
        title: "Music & Animations",
    }, {
        emoji: "📅",
        title: "Schedule Your Post",
    }, {
        emoji: "🗺",
        title: "Templates",
    }, {
        emoji: "🏞",
        title: "Free Stock Assets",
    }, {
        emoji: "📊",
        title: "Manage & Analyse",
    }]

    return (
        <section className="features">
            <h1>Features that you ❤️ in one place</h1>
            <div className="grid">
                {
                    data.map((data, index) => {
                        return <div className="card" key={index}>
                            <span>{data.emoji}</span>
                            <h3>{data.title}</h3>
                            <p>Create marketing copies at scale with our AI powered content generation</p>
                        </div>
                    })
                }
            </div>
        </section>
    )
}

export default Features
