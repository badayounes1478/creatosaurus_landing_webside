import React from 'react'
import '../LandingPageComponentCss/FourthBlackSection.css'

const FourthBlackSection = () => {
    return (
        <div className="fourth-black-section">
            <h1>Built for Creators of Tomorrow</h1>
            <p>Ultimate platofrm built specifically for you, to focus on things that matter.</p>
            <div className="card-container">
                <div className="card">
                    <div className="box">
                        <span>😌</span>
                        <span className="title">Content</span>
                        <p>Create content that impacts people<br />lives Create content that impacts people<br />lives</p>
                    </div>
                </div>

                <div className="card card2">
                    <div className="box">
                        <span>😍</span>
                        <span className="title2">Community</span>
                        <p>Create content that impacts people<br />lives Create content that impacts people<br />lives</p>
                    </div>
                </div>

                <div className="card card3">
                    <div className="box">
                        <span>😇</span>
                        <span className="title3">Culture</span>
                        <p>Create content that impacts people<br />lives Create content that impacts people<br />lives</p>
                    </div>
                </div>

                <div className="card card4">
                    <div className="box">
                        <span>😎</span>
                        <span className="title4">Commerce</span>
                        <p>Create content that impacts people<br />lives Create content that impacts people<br />lives</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FourthBlackSection
