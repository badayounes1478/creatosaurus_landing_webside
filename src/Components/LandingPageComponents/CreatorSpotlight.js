import React from 'react'
import '../LandingPageComponentCss/CreatorSpotlight.css'
import Quote from '../../Assets/Quote.svg'
import ArrowPoint from '../../Assets/ArrowPoint.svg'
import People1 from '../../Assets/BannerPeople1.svg'
import ArrowRight from '../../Assets/ArrowRight.svg'

const CreatorSpotlight = () => {
    return (
        <div className="creator-spotlight">
            <div>
                <h1>Creators spotlight</h1>
                <p>The world's biggest influencers, creators, publishers, startups and brands use <br />Creatosaurus for their creative workflow.</p>
                <div className="email-container">
                    <input type="text" placeholder="Enter your email" />
                    <button>Join the tribe</button>
                </div>
                <img className="quote" src={Quote} alt="" />
                <div className="creator-info">
                    <div>Raj Sharma |</div> <span>Creator</span>
                    <img src={ArrowPoint} alt="" />
                </div>
                <p style={{ fontWeight: 400, marginTop: 10 }}>The world's biggest influencers, creators, publishers, startups and brands use<br />Creatosaurus for their creative workflow.</p>
            </div>
            <div className="profile-container">
                <img className="profile-image" src={People1} alt="" />
                <img className="arrow" src={ArrowRight} alt="" />
            </div>
        </div>
    )
}

export default CreatorSpotlight
